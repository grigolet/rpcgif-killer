FROM ubuntu:latest

WORKDIR /app
RUN apt-get update -y && \
    apt-get install -y python3 python3-pip git && \
    git clone https://gitlab.cern.ch/grigolet/pyhvwrapper.git . && \
    mkdir /configs

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt


RUN pip3 install -r requirements.txt

COPY . /app
COPY configs.example.json /configs/configs.json

USER 1001
EXPOSE 5000

ENTRYPOINT [ "python3", "app.py" ]