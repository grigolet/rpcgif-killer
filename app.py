from flask import Flask, request, jsonify
app = Flask(__name__)
import json
from pyhvwrapper.HVWrapper import HV
from pyhvwrapper.utils import error_codes
import telegram
from threading import Timer
import time
import traceback


with open('/configs/configs.json') as f:
    configs = json.load(f)
    telegram_configs = configs['telegram_configs']
    bot = telegram.Bot(token=telegram_configs['bot_token'])
    chat_id = telegram_configs['chat_id']

hv_configs = configs['hv_configs']

def get_hv_configs(configs, module_name):
    for config in configs:
        if config['module_name'] == module_name:
            return config
    return None

def send_telegram_message(message):
    bot.sendMessage(chat_id, message)

@app.route('/')
def ping():
    return 'Pong'

# Example
# https://rpc-status/stop?module_name=hvrpc256&slot=0&channels=RPC2,RPC4
@app.route('/stop', methods=['POST'])
def stop():
    module_name = request.args.get('module_name')
    if not module_name:
        app.logger.error(f'Could not switch off chamber: module_name param not set')
        send_telegram_message('module_name param not set')
    
    hv_config = get_hv_configs(hv_configs, module_name)
    if not hv_config:
        message = f'Could not find configs for {module_name}'
        app.logger.error(message)
        send_telegram_message(message)
        return '', 400
    slot = int(request.args.get('slot'))
    if not slot:
        app.logger.error(f'Could not switch off chamber: slot param not set')
        send_telegram_message('slot param not set')
        return '', 400
    channels = request.args.get('channels')
    if not channels:
        app.logger.error(f'Could not switch off chamber: channels param not set')
        send_telegram_message('channels param not set')
        return '', 400

    channels = channels.split(',')
    try:
        # Try to parse them to integers
        channels = list(map(int, channels))
    except:
        pass
    hv = HV(ip_address=hv_config['ip_address'],
            sys_type=hv_config['sys_type'],
            user=hv_config['user'],
            password=hv_config['password'],
            lib_path='pyhvwrapper/lib/x64/libcaenhvwrapper.so.5.82')
    result = hv.connect()
    if result != 0: 
        message = f'Could not connect to HV module. Status: {error_codes[result]}'
        app.logger.error(message)
        send_telegram_message(message)
        return '', 400

    result = hv.set_param(slot, channels, param='Pw', value=0)
    if result != 0:
        app.logger.error(f'Could not switch off chamber: error in sending command to the hv module')
        app.logger.error(f'module_name = {module_name}, slot = {slot}, channels = {channels}')
        app.logger.error(f'Error code: {error_codes[result]}')
        send_telegram_message(f'''Could not switch off chamber: error in sending command to the hv module
                                module_name = {module_name}, slot = {slot}, channels = {channels}
                                Error code: {error_codes[result]}''')

        return '', 400

    
    return '', 204
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
